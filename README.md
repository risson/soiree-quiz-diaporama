# Soirée quiz - Diaporama

Run `make slides.pdf` to get the diaporama for the audience.
Run `make handout.pdf` to get the questions and the answers for the presentator.
Run `make` to get both.

###### Known bugs

You may not get the same output in the slides and handout files, if you change your timezone while generating those files.