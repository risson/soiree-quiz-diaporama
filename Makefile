all:: handout.pdf slides.pdf

quick: quiz/* blindtest/* quiz.tex blindtest.tex img/logo.png diaporama.tex slides.tex
	pdflatex -shell-escape slides.tex

slides.pdf: quiz/* blindtest/* quiz.tex blindtest.tex img/logo.png diaporama.tex slides.tex
	pdflatex -shell-escape slides.tex
	pdflatex -shell-escape slides.tex
	pdflatex -shell-escape slides.tex

handout.pdf: quiz/* blindtest/* quiz.tex blindtest.tex img/logo.png diaporama.tex handout.tex
	pdflatex -shell-escape handout.tex
	pdflatex -shell-escape handout.tex
	pdflatex -shell-escape handout.tex

clean:
	rm -f *.toc
	rm -f *.log
	rm -f *.out
	rm -f *.idx
	rm -f *.aux
	rm -f *.nav
	rm -f *.snm
	find . -name "*.tmp" -type f -delete

distclean: clean
	rm -f slides.pdf handout.pdf

.PHONY: all clean distclean
